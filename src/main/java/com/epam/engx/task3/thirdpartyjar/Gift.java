package com.epam.engx.task3.thirdpartyjar;

import javax.annotation.Nonnull;
import java.util.Objects;

public class Gift {

    private final long id;

    @Nonnull
    private final String name;

    @Nonnull
    private final Type type;

    public Gift(long id, String name, Type type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Nonnull
    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Gift gift = (Gift) o;
        return id == gift.id && name.equals(gift.name) && type == gift.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }

    @Override
    public String toString() {
        return "Gift{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
