package com.epam.engx.task4;

import com.epam.engx.task4.thirdpartyjar.InvalidPlanetException;
import com.epam.engx.task4.thirdpartyjar.Planet;
import com.epam.engx.task4.thirdpartyjar.PlanetService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

public class PlanetDensityCalculator {

    private static final Logger LOGGER = LogManager.getLogger(PlanetDensityCalculator.class);

    private final PlanetService planetService;

    public PlanetDensityCalculator(PlanetService planetService) {
        this.planetService = checkNotNull(planetService);
    }

    public double calculatePlanetDensity(String planetName) {
        Planet planet;
        try {
            planet = planetService.findPlanet(planetName);
        } catch (InvalidPlanetException exception) {
            LOGGER.error("Planet not found: {}", planetName, exception);
            return -1;
        }

        return planet.getMass() / calculatePlanetVolume(planet.getRadius());
    }

    double calculatePlanetVolume(double radius) {
        return 4.0 / 3.0 * Math.PI * Math.pow(radius, 3);
    }

}
