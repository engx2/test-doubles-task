package com.epam.engx.task4.thirdpartyjar;

public interface PlanetService {

    Planet findPlanet(String name) throws InvalidPlanetException;

}
