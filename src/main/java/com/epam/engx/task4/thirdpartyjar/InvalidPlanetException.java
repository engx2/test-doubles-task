package com.epam.engx.task4.thirdpartyjar;

public class InvalidPlanetException extends Exception{

    public InvalidPlanetException(String message) {
        super(message);
    }

}
