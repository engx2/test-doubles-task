package com.epam.engx.task4.thirdpartyjar;

import javax.annotation.Nonnull;
import java.util.Objects;

public class Planet {

    @Nonnull
    private final String name;
    private final int mass;
    private final int radius;
    private final int distanceFromSun;

    public Planet(@Nonnull String name,
                  int mass,
                  int radius,
                  int distanceFromSun) {
        this.name = name;
        this.mass = mass;
        this.radius = radius;
        this.distanceFromSun = distanceFromSun;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    public int getMass() {
        return mass;
    }

    public int getRadius() {
        return radius;
    }

    public int getDistanceFromSun() {
        return distanceFromSun;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Planet planet = (Planet) o;
        return mass == planet.mass && radius == planet.radius && distanceFromSun == planet.distanceFromSun && name.equals(planet.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, mass, radius, distanceFromSun);
    }

    @Override
    public String toString() {
        return "Planet{" +
                "name='" + name + '\'' +
                ", mass=" + mass +
                ", radius=" + radius +
                ", distanceFromSun=" + distanceFromSun +
                '}';
    }
}
