package com.epam.engx.task5.thirdpartyjar;

public interface BookService {

    void sell(Book book);

    int getTotalNumberOfSoldBooks();

}
