package com.epam.engx.task5.thirdpartyjar;

import javax.annotation.Nonnull;
import java.util.Objects;

public class Book {

    private final long id;

    @Nonnull
    private final String name;

    public Book(long id, @Nonnull String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Book book = (Book) o;
        return id == book.id && name.equals(book.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
