package com.epam.engx.task5.thirdpartyjar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;

public class DefaultBookService implements BookService {

    private static final Logger LOGGER = LogManager.getLogger(DefaultBookService.class);

    private final AtomicInteger counter = new AtomicInteger();

    @Override
    public void sell(Book book) {
        // do some selling logic
        LOGGER.info("Book {} sold", book);
        counter.incrementAndGet();
    }

    @Override
    public int getTotalNumberOfSoldBooks() {
        return counter.get();
    }
}
