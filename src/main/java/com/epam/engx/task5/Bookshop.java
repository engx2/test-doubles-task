package com.epam.engx.task5;

import com.epam.engx.task5.thirdpartyjar.Book;
import com.epam.engx.task5.thirdpartyjar.BookService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Bookshop {

    private static final Logger LOGGER = LogManager.getLogger(Bookshop.class);

    private final BookService bookService;

    public Bookshop(BookService bookService) {
        this.bookService = bookService;
    }

    public void sellBooks(List<Book> books) {
        books.forEach(bookService::sell);
        LOGGER.info("{} new books sold ({} in total)", books.size(), bookService.getTotalNumberOfSoldBooks());
    }
}
