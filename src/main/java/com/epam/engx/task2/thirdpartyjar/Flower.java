package com.epam.engx.task2.thirdpartyjar;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.Set;

public class Flower {

    private final long id;

    @Nonnull
    private final String name;

    @Nonnull
    private final Set<String> colors;

    public Flower(long id, String name, Set<String> colors) {
        this.id = id;
        this.name = name;
        this.colors = colors;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<String> getColors() {
        return colors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Flower flower = (Flower) o;
        return id == flower.id && Objects.equals(name, flower.name) && Objects.equals(colors, flower.colors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, colors);
    }

    @Override
    public String toString() {
        return "Flower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", colors=" + colors +
                '}';
    }
}
