package com.epam.engx.task2;

import com.epam.engx.task2.thirdpartyjar.Flower;
import com.epam.engx.task2.thirdpartyjar.FlowerRepository;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class FlowerFinder {

    private final FlowerRepository flowerRepository;

    public FlowerFinder(FlowerRepository flowerRepository) {
        this.flowerRepository = checkNotNull(flowerRepository);
    }

    public List<Flower> findByColor(String color) {
        return flowerRepository.findAllFlowers().stream()
                .filter(flower -> flower.getColors().contains(color))
                .toList();
    }
}
